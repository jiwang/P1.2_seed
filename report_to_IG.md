# this is a collective report to Ivan's assignments


## assignment 1, MPI pi
```
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv){
	int mpi_size,mpi_rank;
	
	long n = 100000000;
	long mpi_n;
	long i;
	double x=0;
	double local_pi=0,global_pi=0;
	double w = 1.0/n;
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&mpi_size);
	MPI_Comm_rank(MPI_COMM_WORLD,&mpi_rank);
	
	mpi_n = (long) n/mpi_size;
	if(mpi_rank==mpi_size-1){ 
		mpi_n = n - mpi_n*mpi_rank;
	}
	//fprintf(stdout,"at proc %d, looplength %d\n",mpi_rank,mpi_n);
	for(i=mpi_rank*mpi_n; i<(mpi_rank+1)*mpi_n; ++i){
		x = w*(i+0.5);
		local_pi += (4.0/(1.0 + x*x));
	}
	local_pi *= w;
	//fprintf(stdout,"at proc %d, local_pi %f\n",mpi_rank,local_pi);
	MPI_Reduce(&local_pi,&global_pi,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);	
	
	MPI_Finalize();
	if(!mpi_rank){
		fprintf(stdout,"pi = %f\n",global_pi);
	}	
	
	return 0;
}
```

## assignment 1.5, MPI printing identity matrix to stdout
```
#include<iostream>
#include<mpi.h>
#include<vector>
//#include<memory>

#define mpi_tag 0

using namespace std;

void print(int *,const long int &,const long int &);

void print(int *vec,const long int &dim,const long int &rows){
	for(long int i=0;i<rows*dim;++i){
		cout<<vec[i]<<"\t";
		if((i+1)%dim==0){
			cout<<endl;
		}
	}
}

int main(int argc,char **argv){
	int size,rank;

	const long int dim = 3;
	long int row,len;
	vector<long int> local_row;
	int cue_send=0,cue_recv=0;
	MPI_Request request;
	MPI_Status status;
		
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	// allocate rows to procs
	row = (dim+dim%size)/size;
	len = row*dim;
	for(int i=0;i<size-1;++i){
		local_row.push_back(row);	
	}
	local_row.push_back( dim - row*(size-1) );
	
	// fill contents
	int slice_recv[len];
	int slice_send[len];
	for(int row_idx=0;row_idx<row;++row_idx){
	for(int column_idx=0;column_idx<dim;++column_idx){
		if(column_idx==rank*row+row_idx){
			slice_send[row_idx*dim+column_idx]=1;
		}
		else{
			slice_send[row_idx*dim+column_idx]=0;
		}
	}
	}
	
	//print(slice_send,dim,local_row[rank]);
		
	// print
	if(rank!=0){
		MPI_Isend(slice_send,len,MPI_INT,0,mpi_tag,MPI_COMM_WORLD,&request);
                //cout<<rank<<"send back"<<cue_send<<endl;
	}
	else{
		print(slice_send,dim,local_row[0]);
		for(int i=1;i<size;++i){
			MPI_Recv(slice_recv,len,MPI_INT,i,mpi_tag,MPI_COMM_WORLD,&status);
			print(slice_recv,dim,local_row[i]);
			//cout<<"catch"<<cue_recv<<endl;
		}
	}
	MPI_Finalize();
	
	return 0;
}
```

## assignment 2, OpenMP pi
```
#include<stdio.h>
#include<omp.h>
#include<stdlib.h>
#include<sys/time.h>

double stamp(void){
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return tv.tv_usec*1.0e-6 + tv.tv_sec;
}

int main(int argc, char **argv){
	omp_set_num_threads(atoi(argv[1]));
	
	long i;
	const long n = 5000000000;
	const double w = 1./n;
	double sum = 0.;

	double time = stamp();	
	#pragma omp parallel for reduction(+:sum)
	for(i=0;i<n;++i){
		//fprintf(stdout,"Threads num: %d\n",omp_get_thread_num());
		double x = w*(i+0.5);
		sum += (4./(1.+x*x));
	}
	
	time = stamp() - time;
	const double pi = w*sum;
	fprintf(stdout,"%f, %.16g\n",time,pi);
	return 0;
}
```

## assignment 3, MPI matrix multiplication
```
/*
 * mpi matrix multiplication
 */
#include<iostream>
#include<mpi.h>
#include<stdlib.h>
#include<sys/time.h>
#define mpi_tag 0

using namespace std;

// local_multipication
void local_mult(const double *,const double *,double *,size_t &,size_t &);
// local matrix printing
void print(double *,size_t &,size_t &);
// local time stamp
double stamp(void);

double stamp(void){
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return  tv.tv_sec + tv.tv_usec*1.0e-6;
}

void local_mult(const double *a,const double *b,double *c,size_t &rows,size_t &cols){
    for(size_t i=0;i<rows;++i){
        for(size_t j=0;j<rows;++j){
            c[i*rows+j] = 0;
            // loop through one row in A
            size_t idx {j};
            for(size_t k=i*cols;k<(i+1)*cols;++k){
                c[i*rows+j] += a[k]*b[idx];
                idx += rows;
            }
        }
    }
    //print(c,rows,rows);
}

void print(double *arr,size_t &rows,size_t &cols){
    for(size_t i=0;i<rows*cols;++i){
        cout<<arr[i]<<"\t";
        if((i+1)%cols==0){
            cout<<endl;
        }
    }
}

int main(int argc, char **argv){
    size_t dim {800};
    
    MPI_Init(&argc,&argv);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Request request;
    MPI_Status status;
    
    size_t local_dim = dim/size;
    if(dim%size){
        cout<<"non divisable"<<endl;
        exit(1);
    }
    std::size_t len {dim*local_dim};
    double A[len];
    double C[len];
    double B[len];
    double C_block[size*size];
    double B_block[size*size];
    double B_recv[len];
    // massage passing timer
    double mp_time {0};
    double mp_time_recv {0};
    // operation timer
    double op_time {0};
    double op_time_recv {0};
    
    // initialize A,B
    for(size_t i=0;i<len;++i){
        A[i] = rand()%10;
        B[i] = rand()%10;
        C[i] = 0;
    }
    
    mp_time = stamp();
    // multiplication
    for(int itr=0;itr<size;++itr){
        // load B_block
        size_t tmp {0};
        for(size_t i=0;i<local_dim;++i){
            for(size_t j=itr*local_dim;j<(itr+1)*local_dim;++j){
                B_block[tmp] = B[i*dim+j];
                tmp +=1;
            }
        }
        // all-gather B_block into B_recv
        MPI_Allgather(B_block,local_dim*local_dim,MPI_DOUBLE,B_recv,local_dim*local_dim,MPI_DOUBLE,MPI_COMM_WORLD);
        
        // timer
        double clock = stamp();
        // multiply A by B_recv into C_block
        local_mult(A,B_recv,C_block,local_dim,dim);
        op_time += stamp() - clock;
        
        // push C_block into C
        for(size_t i=0;i<local_dim;++i){
            for(size_t j=0;j<local_dim;++j){
                C[i*dim +itr*local_dim+j] = C_block[i*local_dim +j];
            }
        }
    }
    // timer
    op_time /= size;
    mp_time = (stamp() - mp_time)/size - op_time;
    MPI_Reduce(&mp_time,&mp_time_recv,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    MPI_Reduce(&op_time,&op_time_recv,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    /*
    // print A
    if(rank==0){
        cout<<"printing A"<<endl;
        print(A,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(A,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
    // print B
    if(rank==0){
        cout<<"printing B"<<endl;
        print(B,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(B,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
    // print C
    if(rank==0){
        cout<<"printing C"<<endl;
        print(C,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(C,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
     */
    // print timer
    if(rank==0){
        cout<<"averaged mp time"<<mp_time_recv<<endl;
        cout<<"averaged op time"<<op_time_recv<<endl;
    }
    
    MPI_Finalize();
    
    return 0;
}
```

performance illustration

![mm result](./lectures/ParallelProgramming/hands-on/homework/mpi_mm.png)

![mkl_result](./lectures/ParallelProgramming/hands-on/homework/mpi_mm_mkl.png)

## assignment 4, matrix transpose and multiplication with CUDA
```
#include<iostream>
#include<stdlib.h>

#define matrix_dim 64 //matrix dim

using namespace std;

// reverse matrix arrangement
__global__ void reverse(double *,const size_t &);
// print matrix, given matrix column size, note we handle block matrix here
__host__ void print(const double *,const size_t &);
// multiply two block matrices
__global__ void matmul(const double *,const double *,double *,const size_t &);
__host__ void matmul_host(const double *,const double *,double *,const size_t &);

__global__ void reverse(double *arr,const size_t &size){
	__shared__ double tmp[matrix_dim*matrix_dim];
	tmp[threadIdx.x] = arr[threadIdx.x];
	__syncthreads();
	arr[threadIdx.x] = tmp[size*size-threadIdx.x-1];
}

__host__ void print(const double *arr,const size_t &dim){
	const size_t len = dim*dim;
	for(size_t i=0;i<len;++i){
		cout<<arr[i]<<"\t";
		if( (i+1)%dim==0 )
			cout<<endl;
	}
}

__host__ void matmul_host(const double *a,const double *b,double *c,const size_t &dim){
	for(size_t i=0;i<dim;++i){
		for(size_t j=0;j<dim;++j){
			c[i*dim+j]=0;
			for(size_t k=0;k<dim;++k){
				c[i*dim+j] += a[i*dim+k]*b[j+k*dim];
			}
		}
	}
}


__global__ void matmul(const double *a,const double *b,double *c){
	__shared__ double a_tmp[matrix_dim];
	// allocate a row of A
	a_tmp[threadIdx.x] = a[blockIdx.x*matrix_dim+threadIdx.x];
	c[blockIdx.x*matrix_dim+threadIdx.x] = 0;
	__syncthreads();
	
	// calculate a row of C
	for(size_t k=0;k<matrix_dim;++k){
		c[blockIdx.x*matrix_dim + threadIdx.x] += a_tmp[k]*b[k*matrix_dim+blockIdx.x];
	} 
}

int main(void){
	double * h_a, * h_b, * h_c;
	double * d_a, * d_b, * d_c;
	size_t matrix_size = matrix_dim*matrix_dim;
	h_a = (double *) malloc(matrix_size*sizeof(double));
	h_b = (double *) malloc(matrix_size*sizeof(double));
	h_c = (double *) malloc(matrix_size*sizeof(double));
	cudaMalloc(&d_a, matrix_size*sizeof(double));
	cudaMalloc(&d_b, matrix_size*sizeof(double));
	cudaMalloc(&d_c, matrix_size*sizeof(double));
	for(size_t i=0;i<matrix_size;++i){
		h_a[i] = double(rand()%100);
		h_b[i] = double(rand()%100);
	}
	
	cudaMemcpy(d_a,h_a,matrix_size*sizeof(double),cudaMemcpyHostToDevice);
	cudaMemcpy(d_b,h_b,matrix_size*sizeof(double),cudaMemcpyHostToDevice);
	
	// reverse
	/*
	cout<<"before reverse:"<<endl;
	print(h_a,matrix_dim);	
	reverse<<< 1,matrix_size >>>(d_a,matrix_size);
	cudaMemcpy(h_a,d_a,matrix_size*sizeof(double),cudaMemcpyDeviceToHost);
	cout<<"after reverse:"<<endl;
	print(h_a,matrix_dim);
	*/
	
	// host matmul
	/*
	cout<<"a:"<<endl;
	print(h_a,matrix_dim);
	cout<<"b:"<<endl;
	print(h_b,matrix_dim);
	matmul_host(h_a,h_b,h_c,matrix_dim);
	cout<<"c:"<<endl;
	print(h_c,matrix_dim);
	*/

	// device matmul
	cout<<"a:"<<endl;
	print(h_a,matrix_dim);
	cout<<"b:"<<endl;
	print(h_b,matrix_dim);
	matmul<<< matrix_dim, matrix_dim >>>(d_a,d_b,d_c);
	cudaMemcpy(h_c,d_c,matrix_size*sizeof(double),cudaMemcpyDeviceToHost);
	cout<<"c:"<<endl;
	print(h_c,matrix_dim);
	
	free(h_a);
	free(h_b);
	free(h_c);
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	return 0;
}
```
