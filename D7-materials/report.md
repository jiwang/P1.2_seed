# execise 1, numactl:

- on frontend

```
$ numactl --show

policy: default
preferred node: current
physcpubind: 0 1 2 3 4 5 6 7 8 9 10 11 
cpubind: 0 1 
nodebind: 0 1 
membind: 0 1 
```

so the frontend has 2 sockets with 6 processors in each

```
(for i in `seq 1 6`; do ... ...
```

| NUMA region 0 [Mb/s] | NUMA region 1 [Mb/s] |
|----------------------|----------------------|
|   	12934.0	       |	10408.6       |
|  	22898.6        |	14474.3       |
|   	25455.8        |	13291.4       |
|   	26364.2        |	15304.3       |
|   	26297.8        |	15211.7       |
|	26393.6        |	15206.9       |

one node overall bandwidth is, with no binding, maximum 12 omp threads:
37448.8 Mb/s
one core(processor) bandwidth is, by using seriel code:
12841.6 Mb/s
one core(processor) bandwidth for reading memory associated to the other socket:
9399.8 Mb/s

- on cluster node

cluster node has 2 sockets with 10 processors in each

```
(for i in `seq 1 10`; do ... ...
```

|NUMA region 0 [Mb/s]|NUMA region 1 [Mb/s]|
|--------------------|--------------------|
|	13421.7      |	9890.4            |
|	19334.9      |	17307.7           |
|	20960.7      |	20697.4           |
|	21376.6	     |	21811.7           |
|	21427.4      |	21750.7           |
|	21611.9      |	22037.5           |
|	21229.5      |	22415.8           |
|	21201.6      |	22123.9           |
|	21285.8      |	22176.7           |
|	22613.7      |	21893.1           |

one node overall bandwidth is, with no binding, maximum 20 omp threads:
45457.7 Mb/s
one core(processor) bandwidth is, by using seriel code:
12983.2 Mb/s
one core(processor) bandwidth for reading memory associated to the other socket:
9814.9 Mb/s
one core(processor) bandwidth for reading memory associated to the same socket:
13585.2 Mb/s

- comment
the result is weird on cluster node, where binding processors and memory allocation on the same node does not get significant higher bandwidth.
However from the results above, one core reading memory from the other socket do have lower bandwidth.

# execise 2, latency measure:

- simple trial
```
mpirun -np 2 /u/shared/programs/x86_64/intel/impi_5.0.1/bin64/IMB-MPI1 PingPong
```

| bytes 0 | bytes 1 | bytes 2|
|---------|---------|--------|
| 0.49 | 0.52 | 0.52 |
| 0.57 | 0.64 | 0.64 |
|0.64 |0.68 | 0.67|
|0.56 |0.63 |0.60|

- with given core positions
```
mpirun -np 2 hwloc-bind core:0 core:7 /u/shared/programs/x86_64/intel/impi_5.0.1/bin64/IMB-MPI1 PingPong
```

| bytes 0 | bytes 1 | types 2|
|---------|---------|--------|
| 0.18 | 0.23 | 0.20 |
| 0.20 | 0.22 | 0.21 |
| 0.20 | 0.20 | 0.23 |

```
mpirun -np 2 hwloc-bind core:0 core:12 /u/shared/programs/x86_64/intel/impi_5.0.1/bin64/IMB-MPI1 PingPong
```
| bytes 0 | bytes 1 | types 2|
|---------|---------|--------|
| 0.47 | 0.53 | 0.56 |
| 0.57 | 0.62 | 0.61 |
| 0.51 | 0.54 | 0.57 |

- comments: 
Through binding cores(processors) to different/same sockets we observe that, data transfer between sockets cost more openning time.

# execise 3, HPL with MKL:

- remark

Ulysses's processor has 4+4 flops per cicle ( 4 sums with avx1 and 4 multiplications with avx2),
thus, each core has theoretical peak performance 448 Gflops. 
(well, I got peak performance a little bit larger than that which is confusing)

customized makefile for HPL-2.2 are uploaded, with open_blas and MKL library respectively.

HPL.dat was tuned according to webpage:
http://www.advancedclustering.com/act_kb/tune-hpl-dat-file/

where I pick node=1,ppn=20,mem=160GB

- scalability (weak)

| cores(processors) | N | Nb | P*Q | Gflops | time(s) | settings |
|-------------------|---|----|-----|--------|---------|----------|
| 20|129408 | 192 | 4*5 | 4.484e+2 |3222.24| nodes=1:ppn=20:mem160 |
| 10| 91392 | 192 | 2*5 | 2.313e+2 |2200.31| nodes=1:ppn=10 |
| 5 | 64512 | 192 | 1*5 | 1.167e+2 |1533.70| nodes=1:ppn=5 |
| 1 | 28800 | 192 | 1*1 | 2.585e+1 | 616.23| nodes=1:ppn=1 |

- scalability (strong)

| cores(processors) | N | Nb | P*Q | Gflops | time(s) |
|-------------------|---|----|-----|--------|---------|
| 20 | 30000 | 192 | 4*5 | 3.513e+2 | 51.24  |
| 10 | 30000 | 192 | 2*5 | 2.172e+2 | 82.89  |
| 5  | 30000 | 192 | 1*5 | 1.205e+2 | 149.37 |
| 1  | 30000 | 192 | 1*1 | 2.748e+1 | 655.05 |


comments: both weak and strong scaling meet our expectation roughly,
for analyzing weak scaling, we cosider scaling according to N^2(could be wrong), not sure about the relation between number of operations and matrix size N.

- combination of N, Nb, with nodes=1:ppn=20:mem160

| N | Nb | Gflops | time(s) |
|---|----|--------|---------|
|129408|100|3.388e+2|4264.00|
|129408|300|4.069e+2|3551.11|
|129408|500|4.319e+2|3345.32|
|129408|800|4.216e+2|3426.49|
|129408|1000|4.171e+2|3463.49|
 
