///
/// matrix multiplication
///
#include<iostream>
#include<mpi.h>
#include<stdlib.h>
#include<sys/time.h>
#include<mkl.h>

///
///  mpicxx  -DMKL_ILP64 -m64 -I${MKLROOT}/include mpi_mm_mkl.cc  -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lgomp -lpthread -lm -ldl -o test_mkl
///
#define mpi_tag 0

using namespace std;

// local matrix printing
void print(double *,size_t &,size_t &);
// local time stamp
double stamp(void);

double stamp(void){
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return  tv.tv_sec + tv.tv_usec*1.0e-6;
}

void print(double *arr,size_t &rows,size_t &cols){
    for(size_t i=0;i<rows*cols;++i){
        cout<<arr[i]<<"\t";
        if((i+1)%cols==0){
            cout<<endl;
        }
    }
}

int main(int argc, char **argv){
    size_t dim {1000};
    
    MPI_Init(&argc,&argv);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Request request;
    MPI_Status status;
    
    size_t local_dim = dim/size;
    if(dim%size){
        cout<<"non divisable"<<endl;
        exit(1);
    }
    std::size_t len {dim*local_dim};
    double A[len];
    double C[len];
    double B[len];
    double B_block[size*size];
    double B_recv[len];
    // massage passing timer
    double mp_time {0};
    double mp_time_recv {0};
    // operation timer
    double op_time {0};
    double op_time_recv {0};
    
    // initialize A,B
    for(size_t i=0;i<len;++i){
        A[i] = rand()%100;
        B[i] = rand()%100;
        C[i] = 0;
    }
    
    mp_time = stamp();
    // multiplication
    for(int itr=0;itr<size;++itr){
        // load B_block
        size_t tmp {0};
        for(size_t i=0;i<local_dim;++i){
            for(size_t j=itr*local_dim;j<(itr+1)*local_dim;++j){
                B_block[tmp] = B[i*dim+j];
                tmp +=1;
            }
        }
        // all-gather B_block into B_recv
        MPI_Allgather(B_block,local_dim*local_dim,MPI_DOUBLE,B_recv,local_dim*local_dim,MPI_DOUBLE,MPI_COMM_WORLD);
        
        // timer
        double clock = stamp();
        // alternatively use cblas_dgemm
        cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,local_dim,local_dim,dim,1.0,A,dim,B_recv,local_dim,0.0,C+itr*local_dim,dim);
        op_time += stamp() - clock;
        
    }
    // timer
    op_time /= size;
    mp_time = (stamp() - mp_time)/size - op_time;
    MPI_Reduce(&mp_time,&mp_time_recv,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    MPI_Reduce(&op_time,&op_time_recv,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    
    // print A
    /*
    if(rank==0){
        cout<<"printing A"<<endl;
        print(A,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(A,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
    // print B
    if(rank==0){
        cout<<"printing B"<<endl;
        print(B,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(B,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
    // print C
    if(rank==0){
        cout<<"printing C"<<endl;
        print(C,local_dim,dim);
        double print_recv[len];
        for(int i=1;i<size;++i){
            MPI_Recv(print_recv,len,MPI_DOUBLE,i,mpi_tag,MPI_COMM_WORLD,&status);
            print(print_recv,local_dim,dim);
        }
    }
    else{
        MPI_Isend(C,len,MPI_DOUBLE,0,mpi_tag,MPI_COMM_WORLD,&request);
    }
    */
    // print timer
    if(rank==0){
        cout<<"averaged mp time"<<mp_time_recv<<endl;
        cout<<"averaged op time"<<op_time_recv<<endl;
    }
    
    MPI_Finalize();
    
    return 0;
}

