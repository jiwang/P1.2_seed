#include<iostream>
#include<stdlib.h>

#define matrix_dim 64 //matrix dim

using namespace std;

// reverse matrix arrangement
__global__ void reverse(double *,const size_t &);
// print matrix, given matrix column size, note we handle block matrix here
__host__ void print(const double *,const size_t &);
// multiply two block matrices
__global__ void matmul(const double *,const double *,double *,const size_t &);
__host__ void matmul_host(const double *,const double *,double *,const size_t &);

__global__ void reverse(double *arr,const size_t &size){
	__shared__ double tmp[matrix_dim*matrix_dim];
	tmp[threadIdx.x] = arr[threadIdx.x];
	__syncthreads();
	arr[threadIdx.x] = tmp[size*size-threadIdx.x-1];
}

__host__ void print(const double *arr,const size_t &dim){
	const size_t len = dim*dim;
	for(size_t i=0;i<len;++i){
		cout<<arr[i]<<"\t";
		if( (i+1)%dim==0 )
			cout<<endl;
	}
}

__host__ void matmul_host(const double *a,const double *b,double *c,const size_t &dim){
	for(size_t i=0;i<dim;++i){
		for(size_t j=0;j<dim;++j){
			c[i*dim+j]=0;
			for(size_t k=0;k<dim;++k){
				c[i*dim+j] += a[i*dim+k]*b[j+k*dim];
			}
		}
	}
}


__global__ void matmul(const double *a,const double *b,double *c){
	__shared__ double a_tmp[matrix_dim];
	// allocate a row of A
	a_tmp[threadIdx.x] = a[blockIdx.x*matrix_dim+threadIdx.x];
	c[blockIdx.x*matrix_dim+threadIdx.x] = 0;
	__syncthreads();
	
	// calculate a row of C
	for(size_t k=0;k<matrix_dim;++k){
		c[blockIdx.x*matrix_dim + threadIdx.x] += a_tmp[k]*b[k*matrix_dim+blockIdx.x];
	} 
}

int main(void){
	double * h_a, * h_b, * h_c;
	double * d_a, * d_b, * d_c;
	size_t matrix_size = matrix_dim*matrix_dim;
	h_a = (double *) malloc(matrix_size*sizeof(double));
	h_b = (double *) malloc(matrix_size*sizeof(double));
	h_c = (double *) malloc(matrix_size*sizeof(double));
	cudaMalloc(&d_a, matrix_size*sizeof(double));
	cudaMalloc(&d_b, matrix_size*sizeof(double));
	cudaMalloc(&d_c, matrix_size*sizeof(double));
	for(size_t i=0;i<matrix_size;++i){
		h_a[i] = double(rand()%100);
		h_b[i] = double(rand()%100);
	}
	
	cudaMemcpy(d_a,h_a,matrix_size*sizeof(double),cudaMemcpyHostToDevice);
	cudaMemcpy(d_b,h_b,matrix_size*sizeof(double),cudaMemcpyHostToDevice);
	
	// reverse
	/*
	cout<<"before reverse:"<<endl;
	print(h_a,matrix_dim);	
	reverse<<< 1,matrix_size >>>(d_a,matrix_size);
	cudaMemcpy(h_a,d_a,matrix_size*sizeof(double),cudaMemcpyDeviceToHost);
	cout<<"after reverse:"<<endl;
	print(h_a,matrix_dim);
	*/
	
	// host matmul
	/*
	cout<<"a:"<<endl;
	print(h_a,matrix_dim);
	cout<<"b:"<<endl;
	print(h_b,matrix_dim);
	matmul_host(h_a,h_b,h_c,matrix_dim);
	cout<<"c:"<<endl;
	print(h_c,matrix_dim);
	*/

	// device matmul
	cout<<"a:"<<endl;
	print(h_a,matrix_dim);
	cout<<"b:"<<endl;
	print(h_b,matrix_dim);
	matmul<<< matrix_dim, matrix_dim >>>(d_a,d_b,d_c);
	cudaMemcpy(h_c,d_c,matrix_size*sizeof(double),cudaMemcpyDeviceToHost);
	cout<<"c:"<<endl;
	print(h_c,matrix_dim);
	
	free(h_a);
	free(h_b);
	free(h_c);
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	return 0;
}
