import numpy as np
import matplotlib.pyplot as plt


N = 6

# results with mkl
mp = (0.0028348, 0.00403363, 0.0043444, 0.0044203, 0.00611341, 0.00836157)
op = (0.0245216, 0.0156119, 0.0110506, 0.00768161, 0.00705642, 0.00735313)

# reuslts without mkl
#mp = (0.00570862, 0.00957375, 0.014039, 0.00474885, 0.00702015, 0.0076291)
#op = (0.283143, 0.14457, 0.0786551, 0.0618656, 0.0424342, 0.0314575)

ind = np.arange(N)    # the x locations for the groups
width = 0.35       # the width of the bars: can also be len(x) sequence

p1 = plt.bar(ind, op, width,)
p2 = plt.bar(ind, mp, width,
             bottom=op)

plt.title('with MKL dgemm')
plt.ylabel('Time (s)')
plt.xlabel('MPI processor number')
plt.xticks(ind, ('2', '4', '8', '10', '16', '20'))
#plt.ylim(0,0.3)
#plt.yticks(np.arange(0, 81, 10))
plt.legend((p1[0], p2[0]), ('operation', 'message'))
plt.savefig('mpi_mm_mkl.png')
plt.show()
