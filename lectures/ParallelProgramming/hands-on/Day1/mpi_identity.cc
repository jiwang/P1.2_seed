#include<iostream>
#include<mpi.h>
#include<vector>
//#include<memory>

#define mpi_tag 0

using namespace std;

void print(int *,const long int &,const long int &);

void print(int *vec,const long int &dim,const long int &rows){
	for(long int i=0;i<rows*dim;++i){
		cout<<vec[i]<<"\t";
		if((i+1)%dim==0){
			cout<<endl;
		}
	}
}

int main(int argc,char **argv){
	int size,rank;

	const long int dim = 3;
	long int row,len;
	vector<long int> local_row;
	int cue_send=0,cue_recv=0;
	MPI_Request request;
	MPI_Status status;
		
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	// allocate rows to procs
	row = (dim+dim%size)/size;
	len = row*dim;
	for(int i=0;i<size-1;++i){
		local_row.push_back(row);	
	}
	local_row.push_back( dim - row*(size-1) );
	
	// fill contents
	int slice_recv[len];
	int slice_send[len];
	for(int row_idx=0;row_idx<row;++row_idx){
	for(int column_idx=0;column_idx<dim;++column_idx){
		if(column_idx==rank*row+row_idx){
			slice_send[row_idx*dim+column_idx]=1;
		}
		else{
			slice_send[row_idx*dim+column_idx]=0;
		}
	}
	}
	
	//print(slice_send,dim,local_row[rank]);
		
	// print
	if(rank!=0){
		MPI_Isend(slice_send,len,MPI_INT,0,mpi_tag,MPI_COMM_WORLD,&request);
                //cout<<rank<<"send back"<<cue_send<<endl;
	}
	else{
		print(slice_send,dim,local_row[0]);
		for(int i=1;i<size;++i){
			MPI_Recv(slice_recv,len,MPI_INT,i,mpi_tag,MPI_COMM_WORLD,&status);
			print(slice_recv,dim,local_row[i]);
			//cout<<"catch"<<cue_recv<<endl;
		}
	}
	MPI_Finalize();
	
	return 0;
}

