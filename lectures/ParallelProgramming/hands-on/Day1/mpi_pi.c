#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv){
	int mpi_size,mpi_rank;
	
	long n = 100000000;
	long mpi_n;
	long i;
	double x=0;
	double local_pi=0,global_pi=0;
	double w = 1.0/n;
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&mpi_size);
	MPI_Comm_rank(MPI_COMM_WORLD,&mpi_rank);
	
	mpi_n = (long) n/mpi_size;
	if(mpi_rank==mpi_size-1){ 
		mpi_n = n - mpi_n*mpi_rank;
	}
	//fprintf(stdout,"at proc %d, looplength %d\n",mpi_rank,mpi_n);
	for(i=mpi_rank*mpi_n; i<(mpi_rank+1)*mpi_n; ++i){
		x = w*(i+0.5);
		local_pi += (4.0/(1.0 + x*x));
	}
	local_pi *= w;
	//fprintf(stdout,"at proc %d, local_pi %f\n",mpi_rank,local_pi);
	MPI_Reduce(&local_pi,&global_pi,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);	
	
	MPI_Finalize();
	if(!mpi_rank){
		fprintf(stdout,"pi = %f\n",global_pi);
	}	
	
	return 0;
}
