#include<stdio.h>
#include<omp.h>
#include<stdlib.h>
#include<sys/time.h>

double stamp(void){
	//time_t rawtime;
	//struct tm *info;
	//time(&rawtime);
	//info = localtime(&rawtime);
	//return info->tm_sec + info->tm_min*60;
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return tv.tv_usec*1.0e-6 + tv.tv_sec;
}

int main(int argc, char **argv){
	omp_set_num_threads(atoi(argv[1]));
	
	long i;
	const long n = 5000000000;
	const double w = 1./n;
	double sum = 0.;

	double time = stamp();	
	#pragma omp parallel for reduction(+:sum)
	for(i=0;i<n;++i){
		//fprintf(stdout,"Threads num: %d\n",omp_get_thread_num());
		double x = w*(i+0.5);
		sum += (4./(1.+x*x));
	}
	
	time = stamp() - time;
	const double pi = w*sum;
	fprintf(stdout,"%f, %.16g\n",time,pi);
	return 0;
}
