#include<iostream>
#include<mpi.h>
#include<vector>

#define mpi_tag 0

using namespace std;

void print(std::vector<int> &,const long int &);

void print(std::vector<int> &vec,const long int &dim){
	long int len = vec.size();
	for(long int i=0;i<len;++i){
		cout<<vec[i]<<"\t";
		if((i+1)%dim==0){
			cout<<endl;
		}
	}
}

int main(int argc,char **argv){
	int size,rank;

	const long int dim = 3;
	long int global_row,local_row;
	vector<int> slice;
	int cue_send=0,cue_recv=0;
	int next,prev;
	MPI_Request request;
	MPI_Status status;
		
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	//define next,prev
	next = rank+1;
	prev = rank-1;

	// allocate rows to procs
	local_row = (dim+dim%size)/size;
	global_row = local_row;
	if(rank==size-1){
		local_row = dim - global_row*(size-1);
		//cout<<local_row<<endl;
	}
	//cout<<local_row<<endl;
	
	// fill contents
	for(int row_idx=0;row_idx<local_row;++row_idx){
	for(int column_idx=0;column_idx<dim;++column_idx){
		if(column_idx==rank*global_row+row_idx){
			slice.push_back(1.);
		}
		else{
			slice.push_back(0.);
		}
	}
	}
	// print
	if(rank==0){
		print(slice,dim);
	}
	else{
		MPI_Recv(&cue_recv,1,MPI_INT,prev,mpi_tag,MPI_COMM_WORLD,&status);
		print(slice,dim);
	}
	
	// send finish cue
	if(rank!=size-1){
		MPI_Isend(&cue_send,1,MPI_INT,next,mpi_tag,MPI_COMM_WORLD,&request);
	}

	MPI_Finalize();

	return 0;
}
