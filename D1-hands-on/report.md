# previous home work 
- cpu: Intel i7, Intel(R) Core(TM) i7-5557U CPU @ 3.10GHz [x86 Family 6 Model 61 Stepping 4] 
- freq: 3.1GHz, cores: 2
- GFLOPS/core: 5.35
- peak GFLOPS = 10.70


## weak/strong scaling report
from Jiaxin Wang

# strong scaling case

total sample number = 1e+9

time count for strong scaling

proc=1
```
21.06user 0.01system 0:21.15elapsed 99%CPU
21.08user 0.01system 0:21.17elapsed 99%CPU
21.07user 0.01system 0:21.14elapsed 99%CPU
```
proc=2
```
11.08user 0.01system 0:11.15elapsed 99%CPU
10.90user 0.01system 0:10.96elapsed 99%CPU
10.99user 0.01system 0:11.06elapsed 99%CPU
```
proc=4
```
5.65user 0.02system 0:05.73elapsed 99%CPU
5.61user 0.03system 0:05.69elapsed 99%CPU
5.62user 0.02system 0:05.68elapsed 99%CPU
```
proc=8
```
2.98user 0.03system 0:03.06elapsed 98%CPU
2.93user 0.03system 0:03.04elapsed 97%CPU
2.93user 0.03system 0:03.06elapsed 97%CPU
```
proc=16
```
1.57user 0.07system 0:01.77elapsed 92%CPU
1.57user 0.06system 0:01.72elapsed 95%CPU
1.58user 0.08system 0:01.74elapsed 95%CPU
```

![strong sclaing](./strong.png)

comments: strong scaling is not linear as we though ideally, inter-core information transfer
may drag down the speedup. 

# weak scaling case

time counting

sample number 1e+8 each core

proc=1
```
2.27user 0.01system 0:02.32elapsed 98%CPU
2.25user 0.01system 0:02.31elapsed 98%CPU
2.27user 0.01system 0:02.33elapsed 98%CPU 
```

proc=2
```
2.34user 0.02system 0:02.40elapsed 98%CPU 
2.34user 0.01system 0:02.40elapsed 98%CPU
2.28user 0.01system 0:02.33elapsed 98%CPU 
```
proc=4
```
2.28user 0.02system 0:02.40elapsed 95%CPU
2.29user 0.03system 0:02.38elapsed 97%CPU
2.29user 0.02system 0:02.43elapsed 95%CPU
```
proc=8
```
2.45user 0.03system 0:02.53elapsed 98%CPU
2.39user 0.03system 0:02.48elapsed 97%CPU 
2.41user 0.03system 0:02.49elapsed 98%CPU
```
proc=16
```
2.48user 0.05system 0:02.78elapsed 91%CPU
2.48user 0.05system 0:02.64elapsed 95%CPU 
2.49user 0.06system 0:03.20elapsed 79%CPU
```

![weak scaling](./weak.png)

comment: weak scaling is also not what we expected ideally, I think this time still, the inter-core
information transfer slows the pipeline.
