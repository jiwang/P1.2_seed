### testing report

### TESTS ARE DONE ON ULYSSES FRONTEND

- compiling with -lrt -lm
gcc [shitty code name] -lm -lrt -o test

- with shitty code original
```
./test 100 100
LOOP 0 :: 	0.72026 sec [38141.4]
LOOP 1 :: 	0.678867 sec [38141.4]
LOOP 2 :: 	0.227314 sec [38141.4]
LOOP 3 :: 	0.118894 sec [38141.4]
LOOP 4 :: 	0.0624596 sec [38141.4]
```
-comments:
loop0 is the original loop
in loop1, sqrt is avoided
in loop2, /Ng is replaced by (1./Ng), dividing->multiplying
in loop3, operations belong to upper loop is moved up, in order to save time


- with loop0.c
```
./test 100 100
LOOP 0 :: 	7.05219 sec [381414]
```
-comments:
original loop

- with loop1.c
```
LOOP 1 :: 	6.57003 sec [381414]
```

- with loop2.c
```
LOOP 2 :: 	2.25322 sec [381414]
```

- with loop3.c
```
LOOP 3 :: 	0.902432 sec [381414]
```

- with loop4.c
```
LOOP 4 :: 	0.518517 sec [381414]
```

- with loop5.c
```
LOOP 5 :: 	0.601576 sec [381414]
```
comments:
loop5 is stupid, it separate one step calculation of distance into two steps in each loop

- with loop6.c
```
LOOP 6 :: 	0.465448 sec [381414]
```
comments:
loop6 is faster than loop4, however, this happens only in C, in C++17, register is deprecated


- with loop7.c
```
LOOP 7 :: 	0.456745 sec [381149]
```
loop7 does precalculation, in loop, corresponding value is fetched from precalculated array


- with loop7b.c
```
LOOP 7b :: 	0.463319 sec [381149]
```
loop7b is trying inverse loop, however, this doesn't work out on Ulysses


- with loop8.c
```
LOOP 8 :: 	0.399281 sec [381149]
```
here it's using #pragma ivdep to vectorize the loop, user has to know before that the vectorized operations do not depend on loop

- with loop8b.c
```
LOOP 8b :: 	0.514676 sec [382943]
```
Hmmm, you're introducing something stupid right?
You're trying to vectorizing all 3 loops operations


- with loop8perf.c
```
LOOP 8 :: 	0.396924 sec [381149]
```
Hmmm, this is "correct" improve beyond loop8.c
