# distribute particles report
from Jiaxin Wang

- note that I changed the original code directly

# distribute_v0.c

function ijk, which is designed for searching
reference index is removed, I decided to calculate
index on the fly, which could save some time.

MAS and get_RMax use switch loop instead of for loop

the new loop for points reads
```
// loop over all points
for(p=0; p<Np; ++p){
// over all cells
for(i = 0; i < Ng; i++){
index = i*Ng*Ng;	
dist2 = (x[p] + half_size*(1.0-2.0*i))*(x[p] + half_size*(1.0-2.0*i));
for(j = 0; j < Ng; j++){
index += j*Ng;
dist2 += (y[p] + half_size*(1.0-2.0*j))*(y[p] + half_size*(1.0-2.0*j));
for(k = 0; k < Ng; k++){
index += k;
dist2 += (z[p] + half_size*(1.0-2.0*k))*(z[p] + half_size*(1.0-2.0*k));
//for(p = 0; p < Np; p++){
//dist = sqrt( pow(x[p] - (double)i/Ng + half_size, 2) +
//pow(y[p] - (double)j/Ng + half_size, 2) +
//pow(z[p]  - (double)k/Ng + half_size, 2) );
//if(dist < RMax)
if(dist2 < max2) 
Grid[index] += MAS(dist2);
}
}
}
}
```


