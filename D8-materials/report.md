## HPCG results

```
HPCG-Benchmark version: 3.0
Release date: November 11, 2015
Machine Summary: 
  Distributed Processes: 16
  Threads per processes: 1
Global Problem Dimensions: 
  Global nx: 416
  Global ny: 208
  Global nz: 208
Processor Dimensions: 
  npx: 4
  npy: 2
  npz: 2
Local Domain Dimensions: 
  nx: 104
  ny: 104
  nz: 104
########## Problem Summary  ##########: 
Setup Information: 
  Setup Time: 12.627
Linear System Information: 
  Number of Equations: 17997824
  Number of Nonzero Terms: 482057464
Multigrid Information: 
  Number of coarse grid levels: 3
  Coarse Grids: 
    Grid Level: 1
    Number of Equations: 2249728
    Number of Nonzero Terms: 59774200
    Number of Presmoother Steps: 1
    Number of Postsmoother Steps: 1
    Grid Level: 2
    Number of Equations: 281216
    Number of Nonzero Terms: 7351960
    Number of Presmoother Steps: 1
    Number of Postsmoother Steps: 1
    Grid Level: 3
    Number of Equations: 35152
    Number of Nonzero Terms: 889504
    Number of Presmoother Steps: 1
    Number of Postsmoother Steps: 1
########## Memory Use Summary  ##########: 
Memory Use Information: 
  Total memory used for data (Gbytes): 12.8742
  Memory used for OptimizeProblem data (Gbytes): 0
  Bytes per equation (Total memory / Number of Equations): 715.318
  Memory used for linear system and CG (Gbytes): 11.329
  Coarse Grids: 
    Grid Level: 1
    Memory used: 1.3543
    Grid Level: 2
    Memory used: 0.169576
    Grid Level: 3
    Memory used: 0.0212734
########## V&V Testing Summary  ##########: 
Spectral Convergence Tests: 
  Result: PASSED
  Unpreconditioned: 
    Maximum iteration count: 11
    Expected iteration count: 12
  Preconditioned: 
    Maximum iteration count: 2
    Expected iteration count: 2
Departure from Symmetry |x'Ay-y'Ax|/(2*||x||*||A||*||y||)/epsilon: 
  Result: PASSED
  Departure for SpMV: 2.01225e-09
  Departure for MG: 1.37199e-10
########## Iterations Summary  ##########: 
Iteration Count Information: 
  Result: PASSED
  Reference CG iterations per set: 50
  Optimized CG iterations per set: 50
  Total number of reference iterations: 50
  Total number of optimized iterations: 50
########## Reproducibility Summary  ##########: 
Reproducibility Information: 
  Result: PASSED
  Scaled residual mean: 0.00135652
  Scaled residual variance: 0
########## Performance Summary (times in sec) ##########: 
Benchmark Time Summary: 
  Optimization phase: 0
  DDOT: 1.88312
  WAXPBY: 1.1502
  SpMV: 10.4792
  MG: 57.8164
  Total: 71.3388
Floating Point Operations Summary: 
  Raw DDOT: 5.43534e+09
  Raw WAXPBY: 5.43534e+09
  Raw SpMV: 4.91699e+10
  Raw MG: 2.7477e+11
  Total: 3.3481e+11
  Total with convergence overhead: 3.3481e+11
GB/s Summary: 
  Raw Read B/W: 28.9127
  Raw Write B/W: 6.68159
  Raw Total B/W: 35.5943
  Total with convergence and optimization phase overhead: 34.9752
GFLOP/s Summary: 
  Raw DDOT: 2.88635
  Raw WAXPBY: 4.72558
  Raw SpMV: 4.69212
  Raw MG: 4.75245
  Raw Total: 4.69324
  Total with convergence overhead: 4.69324
  Total with convergence and optimization phase overhead: 4.61162
User Optimization Overheads: 
  Optimization phase time (sec): 0
  Optimization phase time vs reference SpMV+MG time: 0
DDOT Timing Variations: 
  Min DDOT MPI_Allreduce time: 0.130908
  Max DDOT MPI_Allreduce time: 2.00537
  Avg DDOT MPI_Allreduce time: 1.10277
__________ Final Summary __________: 
  HPCG result is VALID with a GFLOP/s rating of: 4.61162
      HPCG 2.4 Rating (for historical value) is: 4.69324
  Reference version of ComputeDotProduct used: Performance results are most likely suboptimal
  Reference version of ComputeSPMV used: Performance results are most likely suboptimal
  Reference version of ComputeMG used: Performance results are most likely suboptimal
  Reference version of ComputeWAXPBY used: Performance results are most likely suboptimal
  Results are valid but execution time (sec) is: 71.3388
       You have selected the QuickPath option: Results are official for legacy installed systems with confirmation from the HPCG Benchmark leaders.
       After confirmation please upload results from the YAML file contents to: http://hpcg-benchmark.org
```
