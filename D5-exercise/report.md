## report of matrix transpose with blocks

I have two stupid methods, code is run on mac

# the stupid method
```
int block_num = matrixdim/blockdim;
for (int l=0; l<block_num+1; ++l){
        // through column blocks
        for (int m=0; m<block_num+1; ++m){
            // inside each block
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                }
            }
        }
    }
```
where we cut big matrix into small elemental blocks

with execution time record
![stupid result](./code_provided/out1.png)

# the more stupid method
```
int block_num = matrixdim/blockdim;
for (int l=0; l<block_num+1; ++l){
        // off diagonal column
        for (int m=l+1;m<block_num+1;++m){
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                    AT[ i*matrixdim + j ] = A[ j*matrixdim + i ];
                }
            }
        }
        
        // diagonal blocks
        for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
            int shift=0;
            for(int j=i+shift;j<(l+1)*blockdim && j<matrixdim;++j){//columns
                AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                AT[ i*matrixdim + j ] = A[ j*matrixdim + i ];
            }
            shift++;
        }
        
    }
```
where we still cut big matrix into small elemental blocsk, but we use half iterations

with execution time record
![more stupid result](./code_provided/out2.png)

# copy sub block method
```
int block[blockdim*blockdim];
128     for (int l=0; l<block_num+1; ++l){
129         // through column blocks
130         for (int m=0; m<block_num+1; ++m){
131             // inside each block
132             for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
133                 for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){
134                     block[(i%blockdim)*blockdim+j%blockdim] = A[i*matrixdim +     j];
135                 }
136             }
137             for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
138                 for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//c    olumns
139                     AT[ j*matrixdim + i ] = block[(i%blockdim)*blockdim+j%blo    ckdim];
140                 }
141             }
142         }
143     }
```

with execution time record
![copy method](./code_provided/out3.png)

# copy and paste method
```
int blocksize = blockdim*blockdim;
149     int block[blocksize];
150     int swap;
151     for (int l=0; l<block_num+1; ++l){
152         // through column blocks
153         for (int m=0; m<block_num+1; ++m){
154             // inside each block
155             for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
156                 for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){
157                     block[(i%blockdim)*blockdim+j%blockdim] = A[i*matrixdim +     j];
158                 }
159             }
160 
161             for(int i=0;i<blocksize;++i){
162                 swap = block[i];
163                 block[i] = block[(i%blockdim)*blockdim + i/blockdim];
164                 block[(i%blockdim)*blockdim + i/blockdim] = swap;
165             }
166 
167             for(int i=m*blockdim;i<(m+1)*blockdim && i<matrixdim;++i){//rows
168                 for(int j=l*blockdim;j<(l+1)*blockdim && j<matrixdim;++j){//c    olumns
169                     AT[ i*matrixdim + j ] = block[(j%blockdim)*blockdim+i%blo    ckdim];
170                 }
171             }
172         }
173     }
```

with execution time record
![copy method](./code_provided/out4.png)
