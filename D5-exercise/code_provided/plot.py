import numpy as np
import matplotlib.pyplot as plt

m = np.loadtxt('out.txt')

block_size = list()
time_mean = list()
time_std = list()
for i in range(0,np.size(m[:,0]-10),10):
	block_size.append(m[i,0])
	time_mean.append(np.median(m[i:i+10,1]))
	time_std.append(np.std(m[i:i+10,1]))

time_mean = np.array(time_mean)
time_std = np.array(time_std)

plt.plot(block_size,time_mean,'k-',label='median')
plt.plot(block_size,time_mean+time_std,'r:',label='$+\sigma$')
plt.plot(block_size,time_mean-time_std,'r:',label='$-\sigma$')

#plt.errorbar(block_size,time_mean,yerr=time_std,fmt='.')
plt.xlabel('sub block size')
plt.ylabel('execution time (s)')
plt.title('transpose block size 2048')
plt.legend(loc=4)
plt.savefig('out.png')
