/*
 * Copyright (C) 2001-2014 The Abdus Salam, International Centre of Theoretical Physics (ICTP)
 *
 * This file is distributed under the terms of the GNU General Public License.
 * See http://www.gnu.org/copyleft/gpl.txt
 *
 * The code is the solution for the exercise D2-exercise2 of the course P1.2 - Master in HPC @SISSA/ICTP
 *
 * Example of code for Matrix transpose with dynamic allocation
 *
 * Author: Ivan Girotto
 * Revised by: G.P. Brandino and M. Atambo
 * Last update: Oct 2014
 *
 */


#include <stdlib.h>
#include <stdio.h>

#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/time.h>


double cclock()
/* Returns elepsed seconds past from the last call to timer rest */
{
    
    struct timeval tmp;
    double sec;
    gettimeofday( &tmp, (struct timezone *)0 );
    sec = tmp.tv_sec + ((double)tmp.tv_usec)/1000000.0;
    return sec;
}

int main( int argc, char * argv [] ) {
    double * A, * AT;
    double t_start, t_end;
    int blockdim, matrixdim;
    
    if( argc < 3 ){
        fprintf( stderr, "Error. The program runs as following: %s [matrixdim] [blockdim].\nProgram exit ...\n", argv[0]);
        exit(1);
    }
    //take dimension of matrix
    matrixdim=atoi(argv[1]);
    //take dimension of block
    blockdim=atoi(argv[2]);
    
    if( matrixdim < 1 || blockdim < 1){
        fprintf( stderr, "Error. Inconsistent parameters.\nProgram exit ...\n");
        exit(1);
    }
    else
    //fprintf( stdout, "Info.\n MatrixSize: %d by %d\n BlockSize: %d by %d\n",matrixdim,matrixdim,blockdim,blockdim);
    // allocate matrix A and transposed AT
    A = ( double * ) malloc( matrixdim * matrixdim * sizeof( double ) );
    AT = ( double * ) malloc( matrixdim * matrixdim * sizeof( double ) );
    
    for(int i = 0; i < matrixdim * matrixdim; ++i ){
        A[i] = (double) i;
    }
    // time counting
    t_start=cclock();
    //-------------------------------------- settings
    // old stupid rectangular trans method
    /*
    // time counting
    t_start=cclock();
    for(int i = 0; i < matrixdim; i++ ){//row
        for(int j = 0; j < matrixdim; j++ ){//column
            AT[ ( j * matrixdim ) + i ] = A[ ( i * matrixdim ) + j ];//swap row,column elements
        }
    }
     */
    
    //--------------------------------------
    int block_num = matrixdim/blockdim;
    // new stupid rectangular trans method
    /*
    // block trans first
    // trhough row blocks
    
    for (int l=0; l<block_num+1; ++l){
        // through column blocks
        for (int m=0; m<block_num+1; ++m){
            // inside each block
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                }
            }
        }
    }
    */
    //--------------------------------------
    // more stupid method
    /*
    for (int l=0; l<block_num+1; ++l){
        // off diagonal column
        for (int m=l+1;m<block_num+1;++m){
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                    AT[ i*matrixdim + j ] = A[ j*matrixdim + i ];
                }
            }
        }
        
        // diagonal blocks
        for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
            int shift=0;
            for(int j=i+shift;j<(l+1)*blockdim && j<matrixdim;++j){//columns
                AT[ j*matrixdim + i ] = A[ i*matrixdim + j ];
                AT[ i*matrixdim + j ] = A[ j*matrixdim + i ];
            }
            shift++;
        }
        
    }
    */
    //--------------------------------------
    // copy small blocks
    /*
    int block[blockdim*blockdim];
    for (int l=0; l<block_num+1; ++l){
        // through column blocks
        for (int m=0; m<block_num+1; ++m){
            // inside each block
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){
                    block[(i%blockdim)*blockdim+j%blockdim] = A[i*matrixdim + j];
                }
            }
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ j*matrixdim + i ] = block[(i%blockdim)*blockdim+j%blockdim];
                }
            }
        }
    }
    */
    //--------------------------------------
    // copy and paste small blocks
    
    int blocksize = blockdim*blockdim;
    int block[blocksize];
    int swap;
    for (int l=0; l<block_num+1; ++l){
        // through column blocks
        for (int m=0; m<block_num+1; ++m){
            // inside each block
            for(int i=l*blockdim;i<(l+1)*blockdim && i<matrixdim;++i){
                for(int j=m*blockdim;j<(m+1)*blockdim && j<matrixdim;++j){
                    block[(i%blockdim)*blockdim+j%blockdim] = A[i*matrixdim + j];
                }
            }
            
            for(int i=0;i<blocksize;++i){
                swap = block[i];
                block[i] = block[(i%blockdim)*blockdim + i/blockdim];
                block[(i%blockdim)*blockdim + i/blockdim] = swap;
            }
            
            for(int i=m*blockdim;i<(m+1)*blockdim && i<matrixdim;++i){//rows
                for(int j=l*blockdim;j<(l+1)*blockdim && j<matrixdim;++j){//columns
                    AT[ i*matrixdim + j ] = block[(j%blockdim)*blockdim+i%blockdim];
                }
            }
        }
    }
    
    //--------------------------------------
    t_end=cclock();
    // print matrix for debug
    /*
    fprintf( stdout,"A\n");
    for(int i=0;i<matrixdim;++i){
        for(int j=0;j<matrixdim;++j){
            fprintf( stdout, "%f\t",A[j*matrixdim+i]);
        }
        fprintf( stdout, "\n");
    }
    fprintf( stdout,"AT\n");
    for(int i=0;i<matrixdim;++i){
        for(int j=0;j<matrixdim;++j){
            fprintf( stdout, "%f\t",AT[j*matrixdim+i]);
        }
        fprintf( stdout, "\n");
    }
    */
    free( A );
    free( AT );
    
    //fprintf( stdout, " Matrix transpose executed. Time Elapsed %9.4f secs\n", t_end-t_start );
    fprintf( stdout, "%d \t %9.4f\n", blockdim,t_end-t_start );
    
    return 0;
}
