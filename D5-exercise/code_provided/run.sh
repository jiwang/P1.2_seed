#!/bin/bash

gcc -std=c11 transpose.c -o transp

MATRIXDIM=$1
BSIZE=2
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
./transp $MATRIXDIM 1 >> out.txt
while [ $BSIZE -lt $((MATRIXDIM/3)) ] && [ $BSIZE -lt 200 ]
do
#echo $BSIZE
#perf stat -e L1-dcache-load-misses ./transp $MATRIXDIM $BSIZE
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
./transp $MATRIXDIM $BSIZE >> out.txt
((BSIZE+=2))
done
python plot.py
rm transp
rm out.txt
